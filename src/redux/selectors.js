export const selectActiveColor = state => state.activeColor;

export const selectSuggestions = state => state.suggestions;

export const selectColors = state => state.colors;

export const selectCurrentSearchPhrase = state => state.currentSearchPhrase;

export const selectColorCandidate = state => state.colorCandidate;
