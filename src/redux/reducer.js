import {SET_COLORS, SET_ACTIVE_COLOR_FROM_CANDIDATE, SET_COLOR_CANDIDATE, SET_CURRENT_SEARCH_PHRASE} from './actionTypes';
import {initialState} from './initialState';
import {findSuggestedColors} from "./providers";

export function reducer(state = initialState, action = {}){
  const {payload} = action;
  switch (action.type){

    case SET_COLORS:
      return {
        ...state,
        colors: payload.colors
      };

    case SET_CURRENT_SEARCH_PHRASE:
      const currentSearchPhrase = payload.currentSearchPhrase;
      const {colors} = state;
      const suggestions = findSuggestedColors(currentSearchPhrase, colors);

      return {
        ...state,
        suggestions,
        currentSearchPhrase,
      };

    case SET_ACTIVE_COLOR_FROM_CANDIDATE:
      const {colorCandidate: activeColor} = state;
      return {
          ...state,
        colorCandidate: {},
        activeColor
      };

    case SET_COLOR_CANDIDATE:
      const {colorCandidate} = payload;
      return {
        ...state,
        suggestions: [],
        currentSearchPhrase: colorCandidate.name,
        colorCandidate: colorCandidate
      };

    default:
      return state;

  }
}