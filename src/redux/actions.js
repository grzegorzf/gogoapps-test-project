import {client} from "../client";
import {SET_COLORS, SET_ACTIVE_COLOR_FROM_CANDIDATE, SET_COLOR_CANDIDATE, SET_CURRENT_SEARCH_PHRASE} from './actionTypes';

export function fetchColors(){

  return function(dispatch){
    return client.getColors()
      .then(response => dispatch(setColors(response)))
  }
}

export function setColors({data}){
  return {
    type: SET_COLORS,
    payload: {colors: data}
  }
}

export function setCurrentSearchPhrase(currentSearchPhrase){
  return {
    type: SET_CURRENT_SEARCH_PHRASE,
    payload: {currentSearchPhrase}
  }
}

export function setActiveColorFromCandidate(){
  return {
    type: SET_ACTIVE_COLOR_FROM_CANDIDATE
  }
}

export function setColorCandidate(colorCandidate){
  return {
    type: SET_COLOR_CANDIDATE,
    payload: {colorCandidate}
  }
}