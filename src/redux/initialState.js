export const baseColor = {name: "white", hex: "ffffff"};
export const blackColor = {name: "black", hex: "000000"};
export const blueColor = {name: "blue", hex: "0000ff"};

export const initialState = {
  colors: [],
  suggestions: [],
  currentSearchPhrase: "",
  activeColor: baseColor,
  colorCandidate: {}
};
