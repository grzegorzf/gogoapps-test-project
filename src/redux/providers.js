import {startsWith} from 'lodash';

const shouldColorBeSuggested = (phrase, {name, hex}) => {
  return startsWith(name, phrase) || startsWith(hex, phrase);
};

export const findSuggestedColors = (selectCurrentSearchPhrase, colors) => {
  if (selectCurrentSearchPhrase.length < 2) return [];
  return colors.filter(color => shouldColorBeSuggested(selectCurrentSearchPhrase, color))
};

