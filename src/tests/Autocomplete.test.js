import React from 'react';
import Enzyme, {mount} from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
Enzyme.configure({ adapter: new Adapter()});
import {expect} from 'chai';
import {Autocomplete, Suggestion} from "../components/color_selection_panel";
import {Button, TextInput} from "../components/color_selection_panel/autocomplete";
import {blackColor, blueColor} from "../redux/initialState";

const MockedAutocomplete = (
  <Autocomplete
      suggestions={[blackColor, blueColor]}
      setSuggestionCandidate={() => {}}
      currentSearchPhrase="bl"
      activeColor={blackColor}
      onSearchPhraseChange={() => {}}
      onSuggestionSelect={() => {}}
  />
);

describe('Autocomplete', () => {
  it('properly renders input and button', () => {
    // when
    const wrapper = mount(MockedAutocomplete);
    // then
    expect(wrapper.find(Button)).to.have.length(1);
    expect(wrapper.find(TextInput)).to.have.length(1);
  });

  it('properly renders suggestion list', () => {
    // when
    const wrapper = mount(MockedAutocomplete);
    // then
    expect(wrapper.find(Suggestion)).to.have.length(2);
  });

  it('properly renders highlighted match of suggested color name', () => {
    // when
    const wrapper = mount(MockedAutocomplete);
    // then
    expect(wrapper.find('.Suggestion p b').first().text()).to.contain("bl");
  });
});