import {reducer, baseColor, blackColor, initialState} from "../redux";
import {SET_COLORS, SET_COLOR_CANDIDATE, SET_ACTIVE_COLOR_FROM_CANDIDATE, SET_CURRENT_SEARCH_PHRASE} from "../redux";
import {expect} from 'chai';

const colors = [baseColor, blackColor];
const initialStateWithColors = {...initialState, colors};

describe('reducer', () => {

  it('should properly set fetched colors', () => {
    const setColorsAction = {type: SET_COLORS, payload: {colors}};
    expect(reducer(initialState, setColorsAction).colors).to.equal(colors);
  });

  it('should properly set current search phrase', () => {
    // given
    const currentSearchPhrase = "bla";
    const setCurrentSearchPhraseAction = {type: SET_CURRENT_SEARCH_PHRASE, payload: {currentSearchPhrase}};
    // when
    const result = reducer(initialStateWithColors, setCurrentSearchPhraseAction);
    // then
    expect(result.currentSearchPhrase).to.equal(currentSearchPhrase);
    expect(result.suggestions.length).to.equal(1);
  });

  it('should properly set color candidate', () => {
    // given
    const setColorCandidateAction = {type: SET_COLOR_CANDIDATE, payload: {colorCandidate: blackColor}};
    // when
    const result = reducer(initialStateWithColors, setColorCandidateAction);
    // then
    expect(result.colorCandidate).to.equal(blackColor);
    expect(result.currentSearchPhrase).to.equal(blackColor.name);
    expect(result.suggestions.length).to.equal(0);
  });

  it('should properly set active color from color candidate', () => {
    // given
    const initialStateWithColorCandidate = {...initialState, colorCandidate: blackColor};
    const setActiveColorFromCandidateAction = {type: SET_ACTIVE_COLOR_FROM_CANDIDATE};
    // when
    const result = reducer(initialStateWithColorCandidate, setActiveColorFromCandidateAction);
    // then
    expect(result.activeColor).to.equal(blackColor);
    expect(result.colorCandidate).to.be.empty;
  });

});