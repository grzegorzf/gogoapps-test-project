import React from 'react';
import Enzyme, {shallow} from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
Enzyme.configure({ adapter: new Adapter()});
import {expect} from 'chai';
import {App} from "../components/app/App";
import {ColorSelectionPanel} from "../components/color_selection_panel";
import {blackColor} from "../redux/initialState";
import hexToRgba from "hex-to-rgba";

describe('App', () => {
  it('properly renders background color', () => {
    //given
    const activeColor = blackColor;
    const expectedBackgroundColor = hexToRgba(activeColor.hex, 0.5);
    // when
    const wrapper = shallow(<App colors={[]} fetchColors={() => {}} activeColor={activeColor}/>);
    // then
    expect(wrapper.prop('style')).to.have.property('backgroundColor', expectedBackgroundColor);
  });

  it('properly renders ColorSelectionPanel when colors are available', () => {
    // when
    const wrapper = shallow(<App colors={[blackColor]} fetchColors={() => {}}/>);
    // then
    expect(wrapper.find(ColorSelectionPanel)).to.have.length(1);

  });

  it('properly renders no colors message when colors are unavailable', () => {
    // when
    const wrapper = shallow(<App colors={[]} fetchColors={() => {}} />);
    // then
    expect(wrapper.find('.no-colors-message')).to.have.length(1);
  });
});