import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import {ConnectedApp} from "./components";
import registerServiceWorker from './registerServiceWorker';
import {Provider} from 'react-redux'
import {store} from "./redux";

ReactDOM.render(
  <Provider store={store}>
    <ConnectedApp />
  </Provider>,
  document.getElementById('root'));
registerServiceWorker();
