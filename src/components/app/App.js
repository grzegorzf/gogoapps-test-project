import React, { Component } from 'react';
import './App.css';
import {ColorSelectionPanel} from "../color_selection_panel";
import hexToRgba from "hex-to-rgba";
import {baseColor} from "../../redux/initialState";

export class App extends Component {

  componentWillMount(){
    const {colors, fetchColors} = this.props;
    if (!colors.length){
      fetchColors();
    }
  }

  getCurrentStyle = () => {
    const {hex} = this.props.activeColor || baseColor;
    return {backgroundColor: hexToRgba(hex, 0.5)}
  };

  renderContent = () => {
    const {colors} = this.props;
    if (!colors.length) return this.renderWaitingForDataMessage();

    return this.renderForm();
  };

  renderForm = () => {
    const {colors, activeColor, colorCandidate, suggestions, setActiveColorFromCandidate, setColorCandidate, currentSearchPhrase, setCurrentSearchPhrase} = this.props;
    return (
      <ColorSelectionPanel
        colors={colors}
        colorCandidate={colorCandidate}
        setSuggestionCandidate={setColorCandidate}
        suggestions={suggestions}
        activeColor={activeColor}
        currentSearchPhrase={currentSearchPhrase}
        setCurrentSearchPhrase={setCurrentSearchPhrase}
        onSuggestionSelect={setActiveColorFromCandidate}
      />
    )
  };

  renderWaitingForDataMessage = () => {
    return <p className="no-colors-message">Waiting for colors...</p>
  };

  render() {
    const {getCurrentStyle, renderContent} = this;

    return (
      <div className="App" style={getCurrentStyle()}>
        <div className="App-content">
          <header className="App-header">
            <h1 className="App-title">GoGo Apps front-end test</h1>
          </header>
          <div className="App-intro">
            {renderContent()}
          </div>
        </div>
      </div>
    );
  }
}


