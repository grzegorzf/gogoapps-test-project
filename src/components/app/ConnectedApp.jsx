import {App} from "./App";
import {connect} from 'react-redux'
import {fetchColors, setActiveColorFromCandidate, setColorCandidate, setCurrentSearchPhrase} from "./../../redux";
import {selectActiveColor, selectColorCandidate, selectCurrentSearchPhrase, selectColors, selectSuggestions} from "../../redux/selectors";

const mapStateToProps = state => (
    {
      colors: selectColors(state),
      currentSearchPhrase: selectCurrentSearchPhrase(state),
      activeColor: selectActiveColor(state),
      colorCandidate: selectColorCandidate(state),
      suggestions: selectSuggestions(state)
    }
);

const mapDispatchToProps = dispatch => (
    {
      fetchColors: () => dispatch(fetchColors()),
      setActiveColorFromCandidate: () => dispatch(setActiveColorFromCandidate()),
      setColorCandidate: color => dispatch(setColorCandidate(color)),
      setCurrentSearchPhrase: searchPhrase => dispatch(setCurrentSearchPhrase(searchPhrase))
    }
);

export const ConnectedApp = connect(
    mapStateToProps,
    mapDispatchToProps,
)(App);
