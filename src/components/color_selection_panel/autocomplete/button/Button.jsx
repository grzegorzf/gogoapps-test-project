import React from 'react';
import './Button.css';
import PropTypes from "prop-types";

export const Button = ({disabled = false, onClick, value}) => (
  <button
    className="Button"
    disabled={disabled}
    onClick={onClick}
    type="button">
    {value}
  </button>
);

Button.propTypes = {
  onClick: PropTypes.func.isRequired,
  value: PropTypes.string.isRequired,
  disabled: PropTypes.bool
};