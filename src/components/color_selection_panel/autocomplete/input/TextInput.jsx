import React from 'react';
import './TextInput.css';

export const TextInput = ({value, onChange, placeholder}) =>(
  <input
    type="text"
    placeholder={placeholder}
    value={value}
    onChange={onChange}
  />
);
