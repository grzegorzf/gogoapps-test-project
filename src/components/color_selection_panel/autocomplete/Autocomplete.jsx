import React from 'react';
import {TextInput} from "./input";
import {Button} from "./button";
import {SuggestionList} from "./suggestions";
import {isObjectEmpty} from "../../../util";
import "./Autocomplete.css"
import PropTypes from "prop-types";

export const Autocomplete = ({suggestions, suggestionCandidate, setSuggestionCandidate, currentSearchPhrase, activeColor, placeholder, onSearchPhraseChange, onSuggestionSelect}) => {
  const isSuggestionCandidateAbsent = isObjectEmpty(suggestionCandidate);
  return (
    <div className="Autocomplete">
      <fieldset>
        <TextInput
            placeholder={placeholder}
            value={currentSearchPhrase}
            onChange={onSearchPhraseChange}
        />
        <Button
            disabled={isSuggestionCandidateAbsent}
            onClick={onSuggestionSelect}
            value="Change to suggested"
        />
      </fieldset>
      <SuggestionList
        currentSearchPhrase={currentSearchPhrase}
        onSuggestionSelect={setSuggestionCandidate}
        suggestions={suggestions}
      />
    </div>
  )
};

Autocomplete.propTypes = {
  suggestions: PropTypes.array.isRequired,
  suggestionCandidate: PropTypes.object,
  setSuggestionCandidate: PropTypes.func.isRequired,
  currentSearchPhrase: PropTypes.string.isRequired,
  activeColor: PropTypes.object.isRequired,
  placeholder: PropTypes.string,
  onSearchPhraseChange: PropTypes.func.isRequired,
  onSuggestionSelect: PropTypes.func.isRequired
};