import React from 'react'
  import './Suggestion.css';
import PropTypes from "prop-types";

export const Suggestion = ({name, currentSearchPhrase, onSuggestionSelect}) => {

  const highlighted = <b>{currentSearchPhrase}</b>;
  const matchPattern = new RegExp(`^${currentSearchPhrase}`);
  const nonHighlighted = name.replace(matchPattern, '');
  const fullColorName = <p>{highlighted}{nonHighlighted}</p>;

  return (
    <div className="Suggestion" onClick={onSuggestionSelect}>
      {fullColorName}
    </div>
  )
};

Suggestion.propTypes = {
  name: PropTypes.string.isRequired,
  onSuggestionSelect: PropTypes.func.isRequired,
  currentSearchPhrase: PropTypes.string.isRequired
};