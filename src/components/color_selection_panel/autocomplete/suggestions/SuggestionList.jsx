import React from 'react';
import {Suggestion} from "./Suggestion";
import './SuggestionList.css'
import PropTypes from "prop-types";

export const mapSuggestionsToSuggestionsList = (suggestions, onSuggestionSelect, currentSearchPhrase) => {
  return suggestions.map((suggestion, index) => {
    return (
      <Suggestion
        key={index}
        currentSearchPhrase={currentSearchPhrase}
        onSuggestionSelect={onSuggestionSelect.bind(this, suggestion)}
        {...suggestion}
      />
    );
  });
};

export const SuggestionList = ({suggestions, onSuggestionSelect, currentSearchPhrase}) => {
  const suggestionsNotEmpty = !!suggestions.length;
  const suggestionListHtml = suggestionsNotEmpty ? mapSuggestionsToSuggestionsList(suggestions, onSuggestionSelect, currentSearchPhrase) : null;
  return (
      <div className={`SuggestionList${suggestionsNotEmpty ? ' filled' : ''}`}>
        {suggestionListHtml}
      </div>
  );
};

SuggestionList.propTypes = {
  suggestions: PropTypes.array.isRequired,
  onSuggestionSelect: PropTypes.func.isRequired,
  currentSearchPhrase: PropTypes.string.isRequired
};