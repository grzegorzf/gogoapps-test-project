import React from 'react';
import {Autocomplete} from "./autocomplete";
import {isObjectEmpty} from "../../util";

export class ColorSelectionPanel extends React.Component {

  onSearchPhraseChange = ({target}) => {
    const {setCurrentSearchPhrase} = this.props;
    const {value} = target;
    setCurrentSearchPhrase(value);
  };

  render (){
    const {suggestions, colorCandidate, setSuggestionCandidate, currentSearchPhrase, activeColor, onSuggestionSelect} = this.props;
    const {onSearchPhraseChange} = this;
    return (
      <div className="ColorSelectionPanel">
        <h6>Currently suggested color: {isObjectEmpty(colorCandidate) ? '-' : colorCandidate.name}</h6>
        <h6>Currently selected color: {activeColor.name}</h6>
        <Autocomplete
            suggestions={suggestions}
            setSuggestionCandidate={setSuggestionCandidate}
            suggestionCandidate={colorCandidate}
            currentSearchPhrase={currentSearchPhrase}
            placeholder="Start typing and then click on one of the colors to select it."
            onSuggestionSelect={onSuggestionSelect}
            onSearchPhraseChange={onSearchPhraseChange}
        />
      </div>
    );
  }
}

