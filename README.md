This project was bootstrapped with [Create React App](https://github.com/facebookincubator/create-react-app).

Small React task delivered in strict time constraints -  see <e>"task_description.pdf"</e> for task details. Technologies used:

React 16.4<br />
Redux<br />
Redux-Thunk<br />
Jest / Enzyme / Chai

To install: <strong>yarn install</strong><br />
To start app: <strong>yarn start</strong><br />
To start tests: <strong>yarn test</strong><br />

1) Due to time constraints, most of the time only "Happy Paths" have been handled.

2) Again, due to time constraints keyboard events have not been handled.

3) Due to minimal nature of task, Redux Thunk has been used instead of Saga.

4) At a first glance an usage of Selectors could seem to be superfluous, but the experience shows that Selectors are always good.
